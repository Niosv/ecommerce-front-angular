import { Component, inject } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { Producto } from '../../models/Producto';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [HttpClientModule, RouterModule],
  templateUrl: './products.component.html',
  styleUrl: './products.component.scss',
})
export class ProductsComponent {
  productos: Producto[] = [];
  productsService = inject(ProductosService);

  ngOnInit() {
    this.productsService.getAllProducts().subscribe((data: Producto[]) => {
      this.productos = data;
    });
    console.log(this.productos);
  }
}
