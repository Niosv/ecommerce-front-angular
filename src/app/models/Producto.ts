export interface Producto {
  id: number;
  nombre: string;
  precio: number;
  categoria: number;
  marca: string;
  stock: number;
  estado: boolean;
  descripcion: string;
  imagen: string;
}
