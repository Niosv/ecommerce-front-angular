import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Producto } from '../models/Producto';

@Injectable({
  providedIn: 'root',
})
export class ProductosService {
  private baseUrl: string = 'http://localhost:3000';

  httClient = inject(HttpClient);

  getAllProducts() {
    return this.httClient.get<Producto[]>(`${this.baseUrl}/producto`);
  }

  getProductById(id: number) {
    return this.httClient.get<Producto>(`${this.baseUrl}/producto/${id}`);
  }

  addProduct(product: Producto) {
    return this.httClient.post<Producto>(`${this.baseUrl}/producto`, product);
  }

  modProduct(product: Producto, id: number) {
    return this.httClient.put<Producto>(`${this.baseUrl}/producto`, product);
  }
}
